from flask import Flask
from config.controller_config import config_controllers
from apscheduler.schedulers.background import BackgroundScheduler
from service.config_service import get_app_port, get_queue_pull_interval_sec
import service.request_queue_processor as request_queue_processor

SCHEDULER = BackgroundScheduler()

app = Flask(__name__)


def init_app():
    config_controllers(app)
    _init_scheduler()

    app_port = get_app_port()
    app.run(host="0.0.0.0", port=app_port)


def _init_scheduler():
    SCHEDULER.add_job(
        func=request_queue_processor.process,
        trigger="interval",
        seconds=get_queue_pull_interval_sec(),
        id="request_queue_processor",
    )

    SCHEDULER.start()
