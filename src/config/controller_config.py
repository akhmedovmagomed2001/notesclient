from controller.general_controller import general_controller
from controller.notes_controller import notes_controller
from controller.queue_controller import queue_controller


def config_controllers(app):
    app.register_blueprint(general_controller)
    app.register_blueprint(notes_controller)
    app.register_blueprint(queue_controller)
