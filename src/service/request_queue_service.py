from service.config_service import get_notes_service_url

BASE_URL = get_notes_service_url()

REQUEST_QUEUE = []
POP_INDEX = -1
PUSH_INDEX = -1


def add_request_into_queue(request_data):
    global PUSH_INDEX

    PUSH_INDEX += 1

    queue_entity = {
        "id": PUSH_INDEX,
        "status": "QUEUED",
        "request_data": request_data,
    }
    REQUEST_QUEUE.append(queue_entity)

    return PUSH_INDEX


def get_queue():
    return REQUEST_QUEUE


def is_queue_empty():
    return PUSH_INDEX == POP_INDEX


def queue_size():
    global PUSH_INDEX, POP_INDEX

    return PUSH_INDEX - POP_INDEX


def get_queue_entity(entity_index):
    queue_entity = {}

    if entity_index > 0 and entity_index <= PUSH_INDEX:
        queue_entity = REQUEST_QUEUE[entity_index]

    return queue_entity


def peek():
    global POP_INDEX
    first_elem_idx = POP_INDEX + 1
    return REQUEST_QUEUE[first_elem_idx]


def pop():
    global POP_INDEX
    POP_INDEX += 1


def mark_as_done(entity_index, response):
    REQUEST_QUEUE[entity_index]["status"] = "DONE"
    REQUEST_QUEUE[entity_index]["response"] = response
