import os

APP_PORT = os.getenv("APP_PORT")
NOTES_SERVICE_URL = os.getenv("NOTES_SERVICE_URL")
QUEUE_PULL_INTERVAL_SEC = float(os.getenv("QUEUE_PULL_INTERVAL_SEC", default=20))


def get_app_port():
    return APP_PORT


def get_notes_service_url():
    return NOTES_SERVICE_URL


def get_queue_pull_interval_sec():
    return QUEUE_PULL_INTERVAL_SEC
