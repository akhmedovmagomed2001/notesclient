import service.request_queue_service as request_queue_service
import client.notes_client as notes_client


def process():
    if not request_queue_service.is_queue_empty():
        queue_entity = request_queue_service.peek()
        entity_id = queue_entity["id"]

        print(f"QueueProcessor: Process entity with id '{entity_id}'")

        request_data = queue_entity["request_data"]
        response = notes_client.get_raw_response(request_data)
        status_code = response.status_code
        if status_code != 429:
            response_message = response.json()
            request_queue_service.mark_as_done(entity_id, response_message)
            request_queue_service.pop()
            print(f"QueueProcessor: Entity with id '{entity_id}' is processed")

            process()
        else: 
            print(f"QueueProcessor: Got 429. Too many requests...")
