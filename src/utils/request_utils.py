from flask import request


def get_request_data():
    json_data = None
    content_length = request.content_length
    if content_length is not None:
        json_data = request.json

    return {
        "method": request.method,
        "headers": dict(request.headers),
        "path": request.path,
        "json": json_data,
    }
