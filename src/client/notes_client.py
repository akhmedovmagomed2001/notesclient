from flask import Blueprint, jsonify
from utils.request_utils import get_request_data
from service.config_service import get_notes_service_url
import requests
import service.request_queue_service as request_queue_service


BASE_URL = get_notes_service_url()


def send_request():
    request_data = get_request_data()
    response = _get_response(request_data)
    return _parse_response_message(response)


def get_raw_response(request_data):
    return _get_response(request_data)


def _get_response(request_data):
    path = request_data["path"]
    url = f"{BASE_URL}{path}"

    response = None
    if request_data["json"] is None:
        response = requests.request(
            method=request_data["method"],
            url=url,
            headers=request_data["headers"],
        )
    else:
        response = requests.request(
            method=request_data["method"],
            url=url,
            headers=request_data["headers"],
            json=request_data["json"],
        )
    return response


def _parse_response_message(response):
    response_message = ""

    status_code = response.status_code
    if status_code == 429:
        request_data = get_request_data()
        queue_entity_id = request_queue_service.add_request_into_queue(request_data)

        queue_message = f"Request is inserted into queue with ID '{queue_entity_id}'"

        response_message = jsonify(message=queue_message)
    else:
        response_message = response.json()

    return response_message
