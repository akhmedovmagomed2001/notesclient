from flask import Blueprint, jsonify
import service.request_queue_service as request_queue_service


queue_controller = Blueprint("queue_controller", __name__)


@queue_controller.route("/queue_entities", methods=["GET"])
def get_notes():
    queue = request_queue_service.get_queue()
    return jsonify(queue)


@queue_controller.route("/queue_entities/<int:id>", methods=["GET"])
def get_note_by_id(id: int):
    queue_entity = request_queue_service.get_queue_entity(id)
    return jsonify(queue_entity)
