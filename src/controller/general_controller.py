from flask import Blueprint, jsonify

general_controller = Blueprint("general_controller", __name__)


@general_controller.route("/", methods=["GET"])
def index():
    response = {"message": "Hello it's notes client!"}

    return jsonify(response)
