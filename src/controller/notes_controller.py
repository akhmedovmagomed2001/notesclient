from flask import Blueprint
import client.notes_client as notes_client


notes_controller = Blueprint("notes_controller", __name__)


@notes_controller.route("/notes", methods=["GET"])
def get_notes():
    return notes_client.send_request()


@notes_controller.route("/notes/<int:id>", methods=["GET"])
def get_note_by_id(id: int):
    return notes_client.send_request()


@notes_controller.route("/notes", methods=["POST"])
def save_note():
    return notes_client.send_request()


@notes_controller.route("/notes/<int:id>", methods=["PUT"])
def update_note(id: int):
    return notes_client.send_request()


@notes_controller.route("/notes/<int:id>", methods=["DELETE"])
def delete_note(id: int):
    return notes_client.send_request()
